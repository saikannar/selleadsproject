package features;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@CucumberOptions(
		features = {"src/test/java/features/Login.feature"},
		glue = {"leafTaps","pages"},
		monochrome = true, //to remove junk characters
		tags = "@test2"
	/*dryRun = true,
		snippets = SnippetType.CAMELCASE*/
		)
@RunWith(Cucumber.class)
public class RunTest {

}
