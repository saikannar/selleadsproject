Feature: Login for Leaftaps

@test2
Scenario Outline: Create Lead
	Given Enter the username as DemoSalesManager
	And Enter the password as crmsfa
	And Click on the login button
	And verify login is success
	And click Leads
	And click Create Lead
	And Enter Company Name as <cpnyname>
	And Enter Firstname as <fname>
	And Enter Surname as <sname>
	And Enter Phone Number as <phno>
	And Enter Email Address as <email>
	When click submitLead
	Then verify Lead created
	Examples:
	|cpnyname|fname|sname|phno|email|
	|HCL|Sai|Kanna|65498797|shaicse2006@gmail.com|
	|Wipro|Balaji|Balaji|987654321|balaji@wipro.com|
