/*package leafTaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class Login {
	public static ChromeDriver driver;
@Given("Launch the Browser")
public void launchBrowser() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(2000,TimeUnit.SECONDS);
	 
}
@Given("Enter URL")
public void enterURL() {
	 
	driver.get("http://leaftaps.com/opentaps/control/main");
}

@Given("Enter the username as (.*)")
public void enterTheUsernameAsDemoSalesManager(String uname) {
driver.findElementById("username").sendKeys(uname);
   
}

@Given("Enter the password as (.*)")
public void enterThePasswordAsCrmsfa(String password) {
			driver.findElementById("password").sendKeys(password);
}

@When("Click on the login button")
public void clickOnTheLoginButton() {
   driver.findElementByClassName("decorativeSubmit").click();
    
}

@Then("verify login is success")
public void verifyLoginIsSuccess() {
   driver.findElementByLinkText("CRM/SFA").click();
   
}

@Then("click Leads")
public void clickLeads() {
    // Write code here that turns the phrase above into concrete actions
    driver.findElementByLinkText("Leads").click();;
}

@Then("click Create Lead")
public void clickCreateLead() {
    // Write code here that turns the phrase above into concrete actions
     driver.findElementByLinkText("Create Lead").click();
}

@Given("Enter Company Name as (.*)")
public void enterCompanyName(String cpnyname) {
    // Write code here that turns the phrase above into concrete actions
    driver.findElementById("createLeadForm_companyName").sendKeys(cpnyname);
}

@Given("Enter Firstname as (.*)")
public void enterFirstname(String fname) {
    // Write code here that turns the phrase above into concrete actions
    driver.findElementById("createLeadForm_firstName").sendKeys(fname);;
}

@Given("Enter Surname as (.*)")
public void enterSurname(String sname) {
    // Write code here that turns the phrase above into concrete actions
     driver.findElementById("createLeadForm_lastName").sendKeys(sname);;
}

@Given("Enter Phone Number as (.*)")
public void enterPhoneNumber(String phno) {
    // Write code here that turns the phrase above into concrete actions
     driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys(phno);
}

@Given("Enter Email Address as (.*)")
public void enterEmailAddress(String email) {
    // Write code here that turns the phrase above into concrete actions
    driver.findElementById("createLeadForm_primaryEmail").sendKeys(email);
}

@When("submit Create Lead")
public void submitCreateLead() {
    // Write code here that turns the phrase above into concrete actions
    driver.findElementByClassName("smallSubmit").click();
}

@Then("verify Lead created")
public void verifyLeadCreated() {
    // Write code here that turns the phrase above into concrete actions
     System.out.println("Created");
}




}
*/