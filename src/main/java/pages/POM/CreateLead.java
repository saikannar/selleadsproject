	package pages.POM;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;
	import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CreateLead extends ProjectMethods {

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using  = "createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID, using  = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using  = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.ID, using  = "createLeadForm_primaryPhoneNumber") WebElement elePhoneNumber;
	@FindBy(how = How.ID, using  = "createLeadForm_primaryEmail") WebElement eleEmailAddress;
	@FindBy(how = How.NAME, using  = "submitButton") WebElement eleSubmit;
	
	@And("Enter Company Name as (.*)")
	public CreateLead companyName(String cname) {
		type(eleCompanyName, cname);
		return this;
	}
	@And("Enter Firstname as (.*)")
	public CreateLead firstName(String fname) {
		type(eleFirstName, fname);
		return this;
	}
	@And("Enter Surname as (.*)")
	public CreateLead lastName(String lname) {
		type(eleLastName, lname);
		return this;
	}
	@And("Enter Phone Number as (.*)")
	public CreateLead phoneNumber(String phno) {
		type(elePhoneNumber, phno);
		return this;
	}
	@And("Enter Email Address as (.*)")
	public CreateLead emailAddress(String emailId) {
		type(eleEmailAddress, emailId);
		return this;
	}
	@When("click submitLead")
	public ViewLead createSubmit() {
		click(eleSubmit);
		return new ViewLead();
	}
	
}
