package pages.POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogin;
	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA") WebElement eleCRM;
	
	@And("verify login is success")
	public MyHome clickCRM() {
		click(eleCRM);
		return new MyHome();
	}
	
	
	/*public LoginPage Logout() {
		click(eleLogin);
		return new LoginPage();
	}*/
}
