package pages.POM;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import wdMethods.SeMethods;

public class ProjectMethods extends SeMethods{

	public String dataSheetName;
//@BeforeMethod(groups= {"any"})
	//@Parameters({"url","username","password"})
@BeforeMethod
//	public void login(String url, String username, String password) throws InterruptedException {
public void login() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("link", "CRM/SFA");
		click(crm);*/
		
	}
/*@AfterMethod
public void closeBrower() {
	closeBrowser();
}
*/
@AfterClass
public void afterClass() {
	System.out.println("@AfterClass");
}
@AfterTest
public void afterTest() {
	System.out.println("@AfterTest");
}
@AfterSuite
public void afterSuite() {
	endResult();
}

@BeforeSuite
public void beforeSuite() {
	startResult();
}

@DataProvider (name = "fetchData")
public Object[][] getData() throws IOException{
	DataInputProvider dp = new DataInputProvider();
	Object[][] sheet = dp.readExcel(dataSheetName);
	return sheet;
}
	
}












