package pages.POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MergeFindLeads extends ProjectMethods {

	public MergeFindLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.NAME, using = "firstName") WebElement eleMergeFirstName;
	@FindBy(how = How.XPATH,using = "//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how = How.XPATH,using = "(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[2]/a") WebElement eleSearchFirstName;
	
	
public MergeLeads mergeLeadFName(String MergeFname) throws InterruptedException {
	type(eleMergeFirstName, MergeFname);
	click(eleFindLeads);
	Thread.sleep(2000);
	clickWithNoSnap(eleSearchFirstName);
	Thread.sleep(3000);
	switchToWindow(0);
	return new MergeLeads();
}

public MergeLeads mergeLeadFName1(String MergeFname, String MergeFname1) throws InterruptedException {
	type(eleMergeFirstName, MergeFname1);
	click(eleFindLeads);
	Thread.sleep(2000);
	//txt = getText(eleSearchFirstName);
	clickWithNoSnap(eleSearchFirstName);
	switchToWindow(0);
	Thread.sleep(3000);
	return new MergeLeads();
}
	

}
