package pages.POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;

public class ViewLead extends ProjectMethods {

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using ="viewLead_firstName_sp") WebElement companyname;

@Then("verify Lead created")
public void viewLead() {
	System.out.println("Lead Created");
}
	
}
