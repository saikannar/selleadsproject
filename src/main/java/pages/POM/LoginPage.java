package pages.POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how  = How.ID, using = "username") WebElement eleUserName;
	@FindBy(how = How.ID, using = "password") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogin;
	
	@Given("Enter the username as (.*)")
	public LoginPage enterUserName(String uName) {
	type(eleUserName, uName);
	return this;
	}
	@And("Enter the password as (.*)")
	public LoginPage enterPassWord (String password) {
		type(elePassword,password);
		return this;
	}
	@And("Click on the login button")
	public HomePage Login() {
		click(eleLogin);
		return new HomePage();
	}
}
