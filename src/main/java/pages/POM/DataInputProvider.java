package pages.POM;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataInputProvider {

	//create Lead
	public static Object[][] readExcel(String dataSheetName) throws IOException {
		//Read excel
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		
		//Go to Sheet
		XSSFSheet sheet = wb.getSheetAt(0);
		
		short cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		Object[][] data = new Object[rowCount][cellCount];
		
		for (int j = 1; j <= rowCount; j++) {
			//Go to row
			XSSFRow row = sheet.getRow(j);
			
			for (int i = 0; i < cellCount; i++) {
				//Go to cell
				XSSFCell cell = row.getCell(i);
				String value = cell.getStringCellValue();
				System.out.println(value);
				data[j-1][i] = value;
			} 
		}
		//Close Excel
		wb.close();
		return data;
	}
//merge Lead
	public static Object[][] readExcel1(String dataSheetName) throws IOException {
		//Read excel
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		
		//Go to Sheet
		XSSFSheet sheet = wb.getSheetAt(1);
		
		short cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		Object[][] data = new Object[rowCount][cellCount];
		
		for (int j = 1; j <= rowCount; j++) {
			//Go to row
			XSSFRow row = sheet.getRow(j);
			
			for (int i = 0; i < cellCount; i++) {
				//Go to cell
				XSSFCell cell = row.getCell(i);
				String value = cell.getStringCellValue();
				System.out.println(value);
				data[j-1][i] = value;
			} 
		}
		//Close Excel
		wb.close();
		return data;
	}
	
	//findLead
	
	public static Object[][] readExcel2(String dataSheetName) throws IOException {
		//Read excel
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		
		//Go to Sheet
		XSSFSheet sheet = wb.getSheetAt(2);
		
		short cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		Object[][] data = new Object[rowCount][cellCount];
		
		for (int j = 1; j <= rowCount; j++) {
			//Go to row
			XSSFRow row = sheet.getRow(j);
			
			for (int i = 0; i < cellCount; i++) {
				//Go to cell
				XSSFCell cell = row.getCell(i);
				String value = cell.getStringCellValue();
				System.out.println(value);
				data[j-1][i] = value;
			} 
		}
		//Close Excel
		wb.close();
		return data;
	}
	
}
