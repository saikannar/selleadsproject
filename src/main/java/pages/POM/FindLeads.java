	package pages.POM;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;
	import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class FindLeads extends ProjectMethods {

	MergeFindLeads mfl = new MergeFindLeads();
	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using  = "//input[@class= ' x-form-text x-form-field ']") WebElement eleLeadID;
	@FindBy(how = How.LINK_TEXT, using  = "Find Leads") WebElement eleFindLeads;
	
	public CreateLead searchLeads() {
		type(eleLeadID,mfl.eleSearchFirstName.getText());
		return new CreateLead();
	}
	
	public CreateLead findLeads() {
		click(eleFindLeads);
		return new CreateLead();
	}

}
