	package pages.POM;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;
	import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MyLeads extends ProjectMethods {

	public MyLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT, using  = "Create Lead") WebElement eleCreateLead;
	@FindBy(how = How.LINK_TEXT, using  = "Find Leads") WebElement eleFindLeads;
	@FindBy(how = How.LINK_TEXT, using  = "Merge Leads") WebElement eleMergeLeads;
	
	@And("click Create Lead")
	public CreateLead createLead() {
		click(eleCreateLead);
		return new CreateLead();
	}
	
	public FindLeads findLeads() {
		click(eleFindLeads);
		return new FindLeads();
	}

	public MergeLeads mergeLeads() {
		click(eleMergeLeads);
		return new MergeLeads();
	}

	
}
