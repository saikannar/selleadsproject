package pages.POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MergeLeads extends ProjectMethods {

	public MergeLeads() {
		PageFactory.initElements(driver, this);
	}
	
@FindBy(how = How.XPATH, using = "(//img[@alt=\"Lookup\"])[1]") WebElement eleFromLookUp;
@FindBy(how = How.XPATH, using = "(//img[@alt=\"Lookup\"])[2]") WebElement eleToLookUp;
@FindBy(how = How.CLASS_NAME,using = "buttonDangerous") WebElement eleMerge;


public MergeFindLeads fromLead() {
	click(eleFromLookUp);
	switchToWindow(1);
	return new MergeFindLeads();
}
	
public MergeFindLeads toLead() {
	click(eleToLookUp);
	switchToWindow(1);
	return new MergeFindLeads();
}

public ViewLead merge() throws InterruptedException {
	clickWithNoSnap(eleMerge);
	acceptAlert();
	return new ViewLead();
	
}

}
