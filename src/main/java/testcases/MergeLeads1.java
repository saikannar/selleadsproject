package testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.POM.LoginPage;
import pages.POM.MergeLeads;
import pages.POM.MyHome;
import pages.POM.ProjectMethods;
import pages.POM.CreateLead;

public class MergeLeads1 extends ProjectMethods {

	@BeforeTest
	public void SetData() {
		/*testCaseName = "Login Logout";
		testNodes = "Login";
		testDescription = "Login into Leaftap";
		category = "smoke";
		authors = "Sai";*/
		dataSheetName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void mergeLeads(String uname, String password,String MergeFname,String MergeFname1) throws InterruptedException {
		new LoginPage()
		.enterUserName(uname)
		.enterPassWord(password)
		.Login()
		.clickCRM()
		.clickLeads()
		.mergeLeads()
		.fromLead()
		.mergeLeadFName(MergeFname)
		.toLead()
		.mergeLeadFName(MergeFname1)
		.merge();
	}
}
