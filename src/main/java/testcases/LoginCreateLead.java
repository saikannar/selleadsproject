package testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.POM.LoginPage;
import pages.POM.MyHome;
import pages.POM.ProjectMethods;
import pages.POM.CreateLead;

public class LoginCreateLead extends ProjectMethods {

	@BeforeTest
	public void SetData() {
		testCaseName = "Login Logout";
		testNodes = "Login";
		testDesc = "Login into Leaftap";
		category = "smoke";
		author = "Sai";
		dataSheetName = "TC001";
	}
	
	@Test(dataProvider = "fetchData")
	public void loginLogOut(String uname, String password,String cname, String fname,String lname,String phno,String emailId) {
		new LoginPage()
		.enterUserName(uname)
		.enterPassWord(password)
		.Login()
		.clickCRM()
		.clickLeads()
		.createLead()
		.companyName(cname)
		.firstName(fname)
		.lastName(lname)
		.phoneNumber(phno)
		.emailAddress(emailId)
		.createSubmit();
	}
}
