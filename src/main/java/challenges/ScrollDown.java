package challenges;

import java.awt.AWTException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ScrollDown {

	public static void main(String[] args) throws InterruptedException, AWTException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.naukri.com/");
		WebElement browseAlljobs = driver.findElementByLinkText("Browse All Jobs");
		boolean displayed = browseAlljobs.isDisplayed();
		System.out.println(displayed);
		// get location
		int y = browseAlljobs.getLocation().getY();
		// scroll to the element
		driver.executeScript("scroll(0,"+y+")");
	}
}