package challenges;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ReadProp {
	static ChromeDriver driver;
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Properties properties = new Properties();
		properties.load(new FileInputStream("./french.properties"));
		String property = properties.getProperty("url");
		System.out.println(property);
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		//driver.findElementById(properties.getProperty("url"));
		//driver.findElementById(property);
		driver.get(property);
		driver.findElementById("username").sendKeys(properties.getProperty("username"), Keys.TAB, 
				properties.getProperty("password"));
		driver.findElementByClassName("decorativeSubmit").click();
	}

}
