package challenges;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;


public class AlertSnapUsingRobotClass {
	
	@Test
	public void takeSnap() throws HeadlessException, AWTException, IOException, InterruptedException{

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();	
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/forgotPassword.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("forgot_passwrd:checkDetails1").click();
		Thread.sleep(1000);
		
		// take snap		
		BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

		ImageIO.write(image, "png", new File("./data/Alert.png"));
		
		
		//Actions crop = new Actions(driver);
		//crop.dragAndDropBy(cropTracker, 30,220).perform();
		
		/*File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("./data/Alert.png"));
		*/
		
		/*Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_WINDOWS);
		rb.keyPress(KeyEvent.VK_PRINTSCREEN);
		rb.keyRelease(KeyEvent.VK_PRINTSCREEN);
		rb.keyRelease(KeyEvent.VK_WINDOWS);*/
	}
}
